#ifdef __TEST_MEMOIZE__
#define MEMOIZE_GETTICKS
#include "memoize.h"
#include "memoize_internal.h"
#include <math.h>


#define RANDOM_NUMBER(a) ((double)rand()/(double)(RAND_MAX/a))
#define RANDOM_INTEGER(a) ((double)((int)RANDOM_NUMBER(a)))
double longfunc(const double x,const double y);
long int __attribute__((const)) fibonacci(const long int n);
long int __attribute__((const)) fibonacci_memoized(struct memoize_hash_t * memo,
                                                   const long int nfib,
                                                   const long int n);

int main (int argc,
          char **  argv)
{
    /*
     * test program for libmemoize
     *
     * In order for memoize to have an impact:
     *
     * The number of saved results in the hash must be
     * approximately the same as the total possible number,
     * or the function must be called repeatedly with the
     * same parameter set.
     *
     * The number of runs must (greatly) exceed the
     * number of possible input parameter combinations.
     */
    printf("Test libmemoize version %s\n",
           MEMOIZE_VERSION);

    /*
     * First set up the memo hash
     */
    struct memoize_hash_t * memo;
    memoize_initialize(&memo);
    if(memo==NULL)
    {
        fprintf(stderr,
                "Failed to allocate memo\n");
        exit(1);
    }
    printf("Initial sizeof memo %zu\n",memoize_sizeof(memo));
    fflush(stdout);
#ifdef MEMOIZE_STATS
    printf("Using MEMOIZE_STATS\n");
#endif//MEMOIZE_STATS
#ifdef MEMOIZE_DEBUG
    printf("Using MEMOIZE_DEBUG\n");
#endif // MEMOIZE_DEBUG

    long int i; // counter
    ticks tstart; // timer
    const long int nfib = 42;

    printf("\n\nRunning memoize Fibonacci test\n\n\n");

    /*
     * Fibonacci test: first without memoize
     */
    tstart = getticks();
    for(i=0;i<=nfib;i++)
    {
        printf("Fibonacci (without memoize) %ld : %ld   \x0d",
               i,
               fibonacci(i));
        fflush(stdout);
    }
    ticks t_fib_no_memoize = getticks() - tstart;
    printf("\nTook % 12g ticks to calculate the first %ld Fibonacci numbers without Memoize\n",
           (double)t_fib_no_memoize,
           nfib
        );
    fflush(stdout);


    /*
     * Provide memoized form of fibonacci
     */

#define Fibbo_memo(x) Memoize(                                          \
        memo,                                                           \
        "fib",                                                          \
        (const size_t)nfib,                                            \
        scalar,                                                         \
        long int,                                                       \
        1,                                                              \
        x,                                                              \
        long int,                                                       \
        1,                                                              \
        fibonacci_memoized(memo,nfib,x))

        tstart = getticks();
    for(i=0;i<=nfib;i++)
    {
        printf("Fibonacci (with memoize   ) %ld : %ld   \x0d",
               i,
               Fibbo_memo(i));
        fflush(stdout);
    }
    ticks t_fib_memoize = getticks() - tstart;
    printf("\nTook % 12g ticks to calculate the first %ld Fibonacci numbers using Memoize\n",
           (double)(t_fib_memoize),
           nfib
        );
    printf("Memoize speed up: %5.2f %%\nMemoize memory cost: %g KB\n",
           (double)t_fib_no_memoize*100.0/(double)t_fib_memoize,
           (double)memoize_sizeof(memo)/(double)1024.0
        );
    fflush(stdout);

    /* reset the memo hash */
    memoize_free(&memo);
    memoize_initialize(&memo);

    /************************************************************/

    /*
     * Now a 2-parameter example using a trigonometric function
     * of two integers.
     *
     * We make the hash big enough to store *every* result, otherwise
     * it's pointless.
     */
    printf("\n\nRunning memoize 2-parameter test\n\n");
    const long int maxi = 10000; // 10000
    const double scale = 10; // 10
    const int nhash = (int)(scale*scale+0.5)+1;

    /*
     * Provide memoized form of longfunc
     */
#define LONGFUNC(x,y) Memoize(                  \
        memo,                                   \
        "long",                                 \
        nhash,                                  \
        array,                                  \
        double,                                 \
        2,                                      \
        ((double[]){x,y}),                      \
        double,                                 \
        1,                                      \
        longfunc(x,y))

    /*
     * test longfunc
     */
    tstart = getticks();
    for(i=0;i<maxi;i++)
    {
        const double x = RANDOM_INTEGER(scale);
        const double y = RANDOM_INTEGER(scale);
        double z = longfunc(x,y);
        z*=1.0;
    }
    printf("Took %g ticks to run %ld longfuncs with random integers between 0 and %d\n",
           (double)(getticks() - tstart),
           maxi,
           (int)(scale+0.5)
        );
    fflush(stdout);

    /*
     * test LONGFUNC
     */
    tstart = getticks();
    for(i=0;i<maxi;i++)
    {
        const double x = RANDOM_INTEGER(scale);
        const double y = RANDOM_INTEGER(scale);
        double z = LONGFUNC(x,y);
        z*=1.0;
    }
    printf("Took %g ticks to run %ld memoized longfuncs with random integers between 0 and %d\n",
           (double)(getticks() - tstart),
           maxi,
           (int)(scale+0.5)
        );
    fflush(stdout);

    /*
     * Test accuracy
     */
    for(i=0;i<maxi;i++)
    {
        const double x = RANDOM_INTEGER(scale);
        const double y = RANDOM_INTEGER(scale);
        const double Z = LONGFUNC(x,y);
        const double z = longfunc(x,y);
        const double eps =
            z!=0.0 ? fabs((z-Z)/z) :
            Z!=0.0 ? fabs((z-Z)/Z) :
            0.0;

        if(eps > 1e-8)
        {
            fprintf(stderr,
                    "(%g,%g) -> %g %g  eps = %g\n",x,y,z,Z,eps);
            exit(3);
        }
    }
    printf("Accuracy tests ok\n");

    /*
     * Free memory
     */
    printf("Final sizeof memo %zu\n",memoize_sizeof(memo));
    memoize_free(&memo);
    fflush(stdout);

    return 0;
}

double __attribute__((const)) longfunc(const double x,const double y)
{
    /*
     * A "long-running" function of x,y.
     *
     * You can adjust imax to make it slow enough.
     */
    double z = x*y;
    int i;
    const int imax = 50000;
    for(i=0;i<imax;i++)
    {
        z += sin((double)i)*cos((double)i);
    }
    return z;
}


/*
 * compute Fibonacci numbers
 */

long int __attribute__((const)) fibonacci(const long int n)
{
    return
        n<2 ?
        n :
        (fibonacci(n-1)+fibonacci(n-2));
}

/*
 * compute Fibonacci numbers in a memoized version:
 * note that this requires some extra data (the memo hash
 * and number of its elements, nfib) to be passed in.
 */

long int __attribute__((const)) fibonacci_memoized(struct memoize_hash_t * memo,
                                                   const long int nfib,
                                                   const long int n)
{
    return
        n<2 ?
        n :
        (Fibbo_memo(n-1)+Fibbo_memo(n-2));
}
#endif //__TEST_MEMOIZE__


typedef int make_iso_compilers_happy;
